// phpcs:ignoreFile

/**
 * @file
 * Integrate Monsido-provided config parsed via regex.
 */

(function (Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.monsido_behavior = {
    attach: function (context, settings) {
      const monsidoTools = drupalSettings.monsidoTools;

      if (monsidoTools.monsidoScriptMatch) {
        const monsidoScriptConfig = JSON.parse(monsidoTools.monsidoScriptMatch);
        window._monsidoConsentManagerConfig = window._monsidoConsentManagerConfig || monsidoScriptConfig;
      }
    }
  }
})(Drupal, drupalSettings);
