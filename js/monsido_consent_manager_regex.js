// phpcs:ignoreFile

/**
 * @file
 * Integrate Monsido-provided Consent Manager config parsed via regex.
 */

(function (Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.monsido_consent_manager_behavior = {
    attach: function (context, settings) {
      const monsidoTools = drupalSettings.monsidoTools;

      if (monsidoTools.enableConsentManager) {
        const monsidoConsentConfig = JSON.parse(monsidoTools.consentScriptMatch);
        window._monsidoConsentManagerConfig = window._monsidoConsentManagerConfig || monsidoConsentConfig;
      }
    }
  }
})(Drupal, drupalSettings);
