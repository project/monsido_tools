<?php

namespace Drupal\monsido_tools\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class MonsidoSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'monsido_tools.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monsido_tools_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['add_monsido_content_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add Content ID to markup'),
      '#default_value' => $config->get('add_monsido_content_id'),
      '#options' => ['' => $this->t('None'), 'meta' => $this->t('Meta Tag')],
      '#description' => $this->t("Include the content ID in the page markup, useful for integration with Monsido's DeepLink capability."),
    ];

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Monsido Scripts'),
      '#description' => $this->t('Activate this field to add tracking your website using Monsido.<br><b>Remember to check your domain settings!</b>'),
      '#default_value' => $config->get('enable'),
    ];

    $form['approach'] = [
      '#type' => 'radios',
      '#title' => $this->t('Script Configuration Approach'),
      '#default_value' => $config->get('approach'),
      '#options' => ['monsido_script' => $this->t('Script from Monsido.com'), 'local' => $this->t('Configure Locally')],
      '#description' => $this->t('Choose whether to paste in a generated script from the Monsido web app, or to configure the features locally. Note that currenly not all features are support in local configuration.'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name=enable]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['monsido_script'] = [
      '#type' => 'textarea',
      '#rows' => 30,
      '#title' => $this->t('Script from Monsido.com'),
      '#default_value' => $config->get('monsido_script'),
      '#description' => $this->t('Paste in the generated script from the Monsido web app. Please provide the entire script as generated.'),
      '#states' => [
        'visible' => [
          ':input[name=enable]' => ['checked' => TRUE],
          ':input[name=approach]' => ['value' => 'monsido_script'],
        ],
        'required' => [
          ':input[name=enable]' => ['checked' => TRUE],
          ':input[name=approach]' => ['value' => 'monsido_script'],
        ],
      ],
    ];

    $form['local'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name=enable]' => ['checked' => TRUE],
          ':input[name=approach]' => ['value' => 'local'],
        ],
      ],
    ];

    $form['local']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#default_value' => $config->get('token'),
      '#states' => [
        'required' => [
          ':input[name=enable]' => ['checked' => TRUE],
          ':input[name=approach]' => ['value' => 'local'],
        ],
      ],
      '#size' => 40,
    ];

    $form['local']['enable_statistics'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Statistics'),
      '#default_value' => $config->get('enable_statistics'),
    ];

    $form['local']['enable_cookieless_tracking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Cookieless Tracking'),
      '#default_value' => $config->get('enable_cookieless_tracking'),
    ];

    $form['local']['enable_document_tracking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Document Tracking'),
      '#default_value' => $config->get('enable_document_tracking'),
      '#description' => $this->t('More options available soon.'),
    ];

    $form['local']['enable_document_tracking_class'] = [
      '#disabled' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Document Class'),
      '#default_value' => $config->get('enable_document_tracking_class'),
      '#description' => $this->t('Search for links, that are associated with class names and contains below mentioned class names.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_document_tracking"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['local']['enable_document_tracking_ignore_class'] = [
      '#disabled' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Document Ignore Class'),
      '#default_value' => $config->get('enable_document_tracking_ignore_class'),
      '#description' => $this->t('Exclude links that contains class name.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_document_tracking"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['local']['add_page_assist_script'] = [
      '#type' => 'radios',
      '#title' => $this->t('PageAssist script'),
      '#default_value' => $config->get('add_page_assist_script'),
      '#options' => [1 => $this->t('Version 1'), 2 => $this->t('Version 2'), 'none' => $this->t('None')],
      '#description' => $this->t('More options available soon for Version 2.'),
      '#states' => [
        'required' => [
          ':input[name=enable]' => ['checked' => TRUE],
          ':input[name=approach]' => ['value' => 'local'],
        ],
      ],
    ];

    $form['local']['add_pagecorrect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add PageCorrect'),
      '#default_value' => $config->get('add_pagecorrect'),
    ];

    $form['local']['add_heatmaps'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Heatmaps'),
      '#default_value' => $config->get('add_heatmaps'),
    ];

    $form['local']['add_monsido_consent_manager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Monsido Consent Manager'),
      '#default_value' => $config->get('add_monsido_consent_manager'),
      '#description' => $this->t('More options available soon.'),
    ];

    $form['local']['monsido_consent_manager_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consent Manager Token'),
      '#default_value' => $config->get('monsido_consent_manager_token'),
      '#states' => [
        'visible' => [
          ':input[name=add_monsido_consent_manager]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name=add_monsido_consent_manager]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['visibility'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Visibility'),
      '#states' => [
        'visible' => [
          ':input[name=enable]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['visibility']['only_front_end'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable scripts only on the <i>front-end</i>'),
      '#default_value' => $config->get('only_front_end'),
      '#description' => $this->t('Only checks admin routes, more options for visibility available soon.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $monsido_script_match = '';
    $consent_script_match = '';
    $approach = $form_state->getValue('approach');
    // Parse the Monsido-generated script, if provided.
    if ($approach == 'monsido_script' && $script = $form_state->getValue('monsido_script')) {
      // Extract the JSON objects from the provided scripts.
      $monsido_regex = '/window\._monsido = window\._monsido \|\| ({[][:™!,"{}#\-\.\w\s]+?});/s';
      $consent_regex = '/window\._monsidoConsentManagerConfig = window\._monsidoConsentManagerConfig \|\| ({[][:,"{}#\-\/\.\w\s]+?});/s';
      $matches = [];
      if (preg_match($monsido_regex, $script, $matches)) {
        // @todo validate the JSON during form validation.
        $monsido_script_match = $matches[1] ?? '';
      }
      $matches = [];
      if (preg_match($consent_regex, $script, $matches)) {
        // @todo validate the JSON during form validation.
        $consent_script_match = $matches[1] ?? '';
      }
      // @todo throw messages if either regex finds no matches.
    }
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('enable', $form_state->getValue('enable'))
      ->set('approach', $form_state->getValue('approach'))
      ->set('monsido_script', $form_state->getValue('monsido_script'))
      ->set('monsido_script_match', $this->jsonFixer($monsido_script_match))
      ->set('consent_script_match', $this->jsonFixer($consent_script_match))
      ->set('token', $form_state->getValue('token'))
      ->set('add_page_assist_script', $form_state->getValue('add_page_assist_script'))
      ->set('enable_statistics', $form_state->getValue('enable_statistics'))
      ->set('enable_cookieless_tracking', $form_state->getValue('enable_cookieless_tracking'))
      ->set('enable_document_tracking', $form_state->getValue('enable_document_tracking'))
      ->set('add_page_assist_script', $form_state->getValue('add_page_assist_script'))
      ->set('add_monsido_content_id', $form_state->getValue('add_monsido_content_id'))
      ->set('add_pagecorrect', $form_state->getValue('add_pagecorrect'))
      ->set('add_heatmaps', $form_state->getValue('add_heatmaps'))
      ->set('add_monsido_consent_manager', $form_state->getValue('add_monsido_consent_manager'))
      ->set('monsido_consent_manager_token', $form_state->getValue('monsido_consent_manager_token'))
      ->set('only_front_end', $form_state->getValue('only_front_end'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to normalize potentially invalid JSON.
   *
   * @param string $json
   *   The input to fix.
   *
   * @return string
   */
  private function jsonFixer($json){
    $patterns     = [];
    /** garbage removal */
    $patterns[0]  = "/([\s:,\{}\[\]])\s*'([^:,\{}\[\]]*)'\s*([\s:,\{}\[\]])/"; //Find any character except colons, commas, curly and square brackets surrounded or not by spaces preceded and followed by spaces, colons, commas, curly or square brackets...
    $patterns[1]  = '/([^\s:,\{}\[\]]*)\{([^\s:,\{}\[\]]*)/'; //Find any left curly brackets surrounded or not by one or more of any character except spaces, colons, commas, curly and square brackets...
    $patterns[2]  =  "/([^\s:,\{}\[\]]+)}/"; //Find any right curly brackets preceded by one or more of any character except spaces, colons, commas, curly and square brackets...
    $patterns[3]  = "/(}),\s*/"; //JSON.parse() doesn't allow trailing commas
    /** reformatting */
    $patterns[4]  = '/([^\s:,\{}\[\]]+\s*)*[^\s:,\{}\[\]]+/'; //Find or not one or more of any character except spaces, colons, commas, curly and square brackets followed by one or more of any character except spaces, colons, commas, curly and square brackets...
    $patterns[5]  = '/["\']+([^"\':,\{}\[\]]*)["\']+/'; //Find one or more of quotation marks or/and apostrophes surrounding any character except colons, commas, curly and square brackets...
    $patterns[6]  = '/(")([^\s:,\{}\[\]]+)(")(\s+([^\s:,\{}\[\]]+))/'; //Find or not one or more of any character except spaces, colons, commas, curly and square brackets surrounded by quotation marks followed by one or more spaces and  one or more of any character except spaces, colons, commas, curly and square brackets...
    $patterns[7]  = "/(')([^\s:,\{}\[\]]+)(')(\s+([^\s:,\{}\[\]]+))/"; //Find or not one or more of any character except spaces, colons, commas, curly and square brackets surrounded by apostrophes followed by one or more spaces and  one or more of any character except spaces, colons, commas, curly and square brackets...
    $patterns[8]  = '/(})(")/'; //Find any right curly brackets followed by quotation marks...
    $patterns[9]  = '/,\s+(})/'; //Find any comma followed by one or more spaces and a right curly bracket...
    $patterns[10] = '/\s+/'; //Find one or more spaces...
    $patterns[11] = '/^\s+/'; //Find one or more spaces at start of string...

    $replacements     = [];
    /** garbage removal */
    $replacements[0]  = '$1 "$2" $3'; //...and put quotation marks surrounded by spaces between them;
    $replacements[1]  = '$1 { $2'; //...and put spaces between them;
    $replacements[2]  = '$1 }'; //...and put a space between them;
    $replacements[3]  = '$1'; //...so, remove trailing commas of any right curly brackets;
    /** reformatting */
    $replacements[4]  = '"$0"'; //...and put quotation marks surrounding them;
    $replacements[5]  = '"$1"'; //...and replace by single quotation marks;
    $replacements[6]  = '\\$1$2\\$3$4'; //...and add back slashes to its quotation marks;
    $replacements[7]  = '\\$1$2\\$3$4'; //...and add back slashes to its apostrophes;
    $replacements[8]  = '$1, $2'; //...and put a comma followed by a space character between them;
    $replacements[9]  = ' $1'; //...and replace by a space followed by a right curly bracket;
    $replacements[10] = ' '; //...and replace by one space;
    $replacements[11] = ''; //...and remove it.

    $result = preg_replace($patterns, $replacements, $json);

    return $result;
  }

}
